#include <Wire.h> //ADC
#include <Adafruit_ADS1X15.h> //ADC
#include <AltSoftSerial.h> //Bluetooth
#include <avr/io.h> //Timer
#include <avr/interrupt.h> //Timer
#include <SoftwareSerial.h>

SoftwareSerial bluetooth(8,9); // TX, RX
Adafruit_ADS1115 ads; //ADC
//AltSoftSerial BTserial; //Bluetooth

int16_t ppg_16bits;

volatile bool timeToSample = false;

void setup(void)
{
  Serial.begin(9600);

  //---------- ADC ----------
  
  ads.begin();
  //ads.setSPS(ADS1115_DR_860SPS);
  ads.startADCReading(ADS1X15_REG_CONFIG_MUX_DIFF_0_1, /*continuous=*/true);

  //---------- Bluetooth ----------
  //BTserial.begin(115200);
  bluetooth.begin(9600);


  //---------- Interrupt ----------

  //stop interrupts
  cli();
  
  //TCCR2A = COM2A1, COM2A0, COM2B1, COM2B0, -, -, WGM21, WGM20
  TCCR2A = 0b00000010; //WGM01:0 = 10 -> CTC mode of operation
  
  //TCCR2B = FOC2A, FOC2B, -, -, WGM22, CS22, CS21, CS20
  TCCR2B = 0b00000111; //CS02:0 = 111 -> prescaler 1024
  
  //initialize counter value to 0
  TCNT2  = 0;
  
  // set compare match register for 100 Hz increments
  OCR2A = 155;// = (16*10^6) / (100*1024) - 1 (must be <256)

  // enable timer compare interrupt
  TIMSK2 = 0b00000010;

  //allow interrupts
  sei();
}

//interrupt de timer 2
ISR (TIMER2_COMPA_vect)
{
  timeToSample = true;
}

void loop(void)
{
  if(timeToSample){
    ppg_16bits = ads.getLastConversionResults();
    timeToSample = false;

    Serial.println(ppg_16bits);
    bluetooth.println(ppg_16bits);
  }
}
